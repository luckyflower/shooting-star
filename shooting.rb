#     shooting-star
#     Copyright (C) 2024  Aaron Brown
#
#     This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# if you HAVE TO contact me, my email is luckyenterr@protonmail.com
require 'json'
require 'sqlite3'
require 'openssl'
def bop text
	return OpenSSL::Digest::MD5.hexdigest text
end
session = bop rand.to_s
puts "✅ session #{session} ready"
code = ""
loop do
	input = gets
	if input == nil then break end
	code += input
end
chain = SQLite3::Database.open("chain")
puts "⛏ mining block on #{session}"
req = JSON.parse code
leep = bop (chain.execute "select * from blocks").to_s
if req["type"] == "store"
	prev = bop req["data"]
	data = req["data"]
	chain.execute "insert into blocks values('#{prev}','#{leep}','#{data.gsub("'","\\'")}');"
end
if req["type"] == "sync"
	system "cat chain|nc #{req["dropoff"]}"
end
if req["type"] == "contract"
	n = File.popen "ruby contractvm.rb","w"
	n.puts req["data"]
	n.close
end
puts "🪙 mined block on #{session}"
