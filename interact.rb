#     shooting-star
#     Copyright (C) 2024  Aaron Brown
#
#     This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# if you HAVE TO contact me, my email is luckyenterr@protonmail.com
if not ARGV[0] then
	puts "failed!"
	exit
end
printf ">> "
x = gets.chomp + ";"
system "touch interaction.c"
z = File.open "interaction.c","w"
z.puts "int main() {"
z.puts x
z.puts "return 0;"
z.puts "}"
z.close
system "musl-gcc contract.c interaction.c -o interaction"
system "./interaction"
system "rm -f ./interaction interaction.c"
