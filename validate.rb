#     shooting-star
#     Copyright (C) 2024  Aaron Brown
#
#     This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# if you HAVE TO contact me, my email is luckyenterr@protonmail.com
require 'sqlite3'
require 'openssl'
x = SQLite3::Database.open "chain"
ex = 0
for i in x.execute "select * from blocks"
	printf "\r🔍 hashchecking #{i[0]}"
	if (i[0] != OpenSSL::Digest::MD5.hexdigest(i[2])) then
		puts "INCORRECT HASH: #{i[0]}"
	end
	sleep 0
	ex += 1
end
if ex > 0
	puts ""
end
virtual = Array.new
for i in x.execute "select * from blocks"
	printf "\r⚙ validating #{i[0]}"
	if (i[1] != OpenSSL::Digest::MD5.hexdigest(virtual.to_s)) then
		puts "INCORRECT BLOCK: #{i[0]}"
	end
	virtual.push i
	sleep 0
	ex += 1 
end
if ex > 0
	puts ""
end
x.close
puts "🧊 done validating!"
